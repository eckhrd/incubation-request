# Authors

This application to The Carpentries' Lesson Program Incubator was crafted by
the following members of the HPC Carpentry Community:

- Alan O'Cais ([@ocaisa](https://github.com/ocaisa))
- Andrew Reid ([@reida](https://github.com/reida))
- Annajiat Alim Rasel ([@annajiat](https://github.com/annajiat))
- Benson Muite ([@bkmgit](https://github.com/bkmgit))
- Rohit Goswami ([@HaoZeke](https://github.com/HaoZeke))
- Trevor Keller ([@tkphd](https://github.com/tkphd))
- Wirawan Purwanto ([@wirawan0](https://github.com/wirawan0))
