# Application for HPC Carpentry Incubation

This application to The Carpentries' Lesson Program Incubator was crafted
by the following members of the HPC Carpentry Community:

- Alan O'Cais ([@ocaisa]), [CECAM], Spain
- Andrew Reid ([@reid-a]), [NIST], USA
- Annajiat Alim Rasel ([@annajiat]), [BracU], Bangladesh
- Benson Muite ([@bkmgit]), [Kichakato Kizito], Kenya
- Rohit Goswami ([@haozeke]), [Quansight Labs] & [U. Iceland]
- Trevor Keller ([@tkphd]), [NIST], USA
- Wirawan Purwanto ([@wirawan0]), [ODU], USA

## Requirements

The Carpentries' [Lesson Development Handbook][ldh] requests that the
application document covers the following items _at a minimum_:

1. Description of the community that is working on the Lesson Project
2. Description of domain knowledge required for instructors to teach the
   material, so that we can determine to what extent our existing
   instructor pool may possess these skills and be able to teach these
   workshops
3. Description of the community that the Lesson Project serves with their
   workshops
4. Goals of the Lesson Project
5. List of the lessons developed or under development and what a two-day
   workshop would include, along with access to the materials for
   Carpentries review
6. Information about when and where at least some of the materials have
   been taught, even if as smaller modules and not yet a full-fledged 2-day
   workshop
7. Assessment information from the teaching events regarding number of
   attendees and learner feedback about the workshop(s), either using The
   Carpentries standard surveys or custom survey questions
8. How the Lesson Project feels that becoming an official Carpentries
   Lesson Program will benefit the Lesson Project, The Carpentries, and the
   broader learning community.

Including this list at the end, with links back to the relevant clauses,
would be helpful. Answering in the form of an itemized list would not be
helpful.

<!-- handles -->
[@annajiat]: https://github.com/annajiat
[@bkmgit]:   https://github.com/bkmgit
[@haozeke]:  https://github.com/haozeke
[@ocaisa]:   https://github.com/ocaisa
[@reid-a]:   https://github.com/reid-a
[@tkphd]:    https://github.com/tkphd
[@wirawan0]: https://github.com/wirawan0

<!-- affiliations -->
[BracU]: https://www.bracu.ac.bd
[CECAM]: https://www.cecam.org
[Kichakato Kizito]: http://kichakatokizito.solutions
[NIST]: https://www.nist.gov
[ODU]: https://www.odu.edu
[Quansight labs]: https://labs.quansight.org
[U. Iceland]: https://english.hi.is

<!-- links -->
[ldh]: https://docs.carpentries.org/topic_folders/governance/lesson-program-policy.html#lesson-programs
