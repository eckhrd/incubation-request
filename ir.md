---
author:
  - Andrew Reid ([@reida]), [NIST], USA
  - Trevor Keller ([@tkphd]), [NIST], USA
  - Annajiat Alim Rasel ([@annajiat]), [BracU], Bangladesh
  - Benson Muite ([@bkmgit]), [Kichakato Kizito], Kenya
  - Alan O'Cais ([@ocaisa]), [CECAM], Spain
  - Wirawan Purwanto ([@wirawan0]), [ODU], USA
  - Rohit Goswami ([@haozeke]), [Quansight Labs] & [U. Iceland]
---

# HPC Carpentry and The Carpentries

"High-performance computing" or HPC refers to clusters of computers,
interconnected by low-latency networks, used to perform parallel
computing tasks that are intractible on personal computer resources.
Examples include large-scale fluid dynamics simulations ("virtual
windtunnels") that require many terabytes of memory capacity and the
concerted efforts of thousands of processors to complete in a
reasonable amount of time, training of large machine learning models
that require the concerted effort of thousands of general-purpose
graphics processing units (GPUs) to perform the required linear
algebra, and countless varieties of simultaneous program execution to
sweep a parameter space, enumerate crystal or protein structures, or
integrate systems of differential equations that defy analytical
solution.

HPC resources are scientific instruments requiring institutional
investment to obtain and training to effectively use.
As off-the-shelf hardware has improved in quality and fallen in price
over the decades, the leverage such machinery can apply to scientific
discovery has both increased and become more accessible to researchers
at institutions great and small.
Simultaneously, the operating systems and interfaces of personal
computing machinery have introduced an ever-deepening stack of
abstractions between the user and the hardware.
These two trends conspire to make the learning curve for new HPC users
ever steeper:
you must use computational resources to perform almost any research
task, yet the modes of interaction native to most HPC clusters are
entirely foreign to anyone raised in the era of smartphones and web
portals.

The core concept behind HPC Carpentry is to close this gap.

## Who We Are

The HPC Carpentry project is a group of volunteers who are working on
developing a number of lessons, closely aligned with the goals of The
Carpentries generally, for the specific use-case of novice users of
high-performance computing resources.
These resources offer the benefit of access to large sets of compute
resources, and benefits arising from the ability to run computational
tasks in parallel, but also require a different skill set than that
associated with the kind of serial computations typically done on
laptops or local workstations.

Our developer community consists of a number of contributors with
domain expertise in the HPC world, and a number who are in educational
roles at universities, with some overlap.
This community has persisted across at least one major leadership
change (the departure of Peter Steinbach from a central role in the
effort), and has a wide range of informal connections to HPC providers
and site operators, such as Compute Canada.

## Governance and Lesson Improvement 

The HPC Carpentry group has a steering committee which is formally
elected from the larger group by a GitHub-mediated process. The
steering committee nominally controls who has write access to the
lessons and can approve pull requests, but in practice contributors
have mostly been drawn from a small group, and approval has been
collegial and relatively informal. The steering committee also
sets strategic goals, primarily by consensus, and coordinates
community activities like the recent SC21 BoF drafts, and the
composition of this document.

Our lesson improvement model is essentially that of The Carpentries
generally. Our lessons are in GitHub repositories, and we collect
positive and negative feedback from workshop organizers, instructors,
and learners after workshops, and make an effort to translate this
feedback into actionable issues on the repositories. 

## Our Goals

The principal goal of the effort is to make both beginner-friendly and
accurate technical material available to new users of HPC resources.
This will improve training outcomes for users, ease the user
on-boarding burden for site operators, and help democratize HPC usage
in the face of more widespread and cheaper computational resources.
Our lessons are public, free, and open-source, and we aspire to the
kind of continuous improvement that arises from learner and instructor
feedback that is characteristic of The Carpentries.

## Existing Content and Strategic Plan

Currently, we have two major lessons at a reasonably mature stage of
development.
They are [HPC Intro], which introduces learners to basic operations on
shared HPC resources, from remote log-in to parallel job submission;
and [HPC Workflows], which uses the Snakemake tool to demonstrate
workflow automation on HPC systems while reinforcing core concepts
from The Carpentries' Python and Shell lessons.
HPC Intro has been taught many times by members of HPC Carpentry 
around the world, both as add-ons to Carpentries-organized workshops
and at stand-alone HPC Carpentry events.

Our strategic plan has these lessons as part of two types of workshop:
one centered around users, and another centered around prospective HPC
code developers.
The user workshop will make use of the existing Unix Shell lesson,
followed by the HPC Intro lesson, and then follow up with the workflow
lesson.
The developer workshop will start the same way, with the Unix Shell
and HPC Intro, but will follow with a yet-to-be-developed lesson about
how to write codes that run well on HPC systems.

In addition to the lessons intended for the workshops, we have a
variety of other content that covers tangentially related issues,
including the Chapel programming language, and some GPU programming
content.  (Is this in fact true?)

## HPC Workshop Requirements

### Resources

During an HPC Carpentry lesson, it will be necessary for learners to
access an actual HPC cluster, whether it is a permanent installation or
an ephemeral instance for instructional purposes. Lesson organizers
may need to coordinate with an HPC site operator, or may need to 
stand up an instructional cluster on cloud resources. We are aware
of tools which accomplish this latter task, but access to the 
necessary cloud resources may be an issue.

### Instructor Knowledge

Lesson organizers will also need to ensure that learners can log in,
possibly including creating accounts for learners. Instructors should
be familiar with the layout and peculiarities of the particular cluster
they are using, so that their live-typing procedure will actually work,
and to deal with anomalies learners may encounter. Instructors do not
need to be HPC experts themselves, but ideally should be experienced
users of clusters in general, and have a basic familiarity with the
instructional cluster.

### Lesson Customization

In addition, because of the variable nature of HPC resources, and the
requirement that instructors will live-type commands that are appropriate
to the resource, our lessons must be configurable so that references
to the cluster head node or other site-specific resources are correct.
To this end, our lessons incorporate a "snippet library" which can
be customized on a per-site basis. This is a deviation from the usual
Carpentries lesson structure.


## State of the HPC Carpentry Community

The HPC Carpentry Steering Committee is elected annually following a
GitHub-based asynchronous democratic process. The current members are:

- Alan O'Cais, [CECAM], Spain
- Andrew Reid, [NIST], USA
- Annajiat Alim Rasel, [BracU], Bangladesh
- Rohit Goswami, [Quansight Labs] & [U. Iceland]
- Trevor Keller, [NIST], USA
- Wirawan Purwanto, [ODU], USA

Members of the Steering Committee organize public meetings twice
monthly to coordinate and execute on shared goals, write abstracts and
proposals for community outreach (including sessions at CarpentryCon),
and contribute to curriculum and lesson development efforts through
the GitHub-based workflows typical of Carpentries projects.

Coordination and communication between community members occurs on
[GitHub][hpcc-gh] (31 members), [Slack][hpcc-sl] (215 members),
[TopicBox][hpcc-tb], [Twitter][hpcc-tw] (56 followers), and through
the HPC Carpentry [website][hpcc-web].
Over the past year, HPC Carpentry [workshops][hpcc-workshops] have
been taught in Dublin, Ireland; Miami, Florida, USA; Dhaka,
Bangladesh; Berlin, Germany; Moka, Mauritius; and Gaithersburg,
Maryland, USA.

## This Is a Good Idea

Becoming an official Carpentries lesson will enrich The Carpentries
portfolio by adding HPC content in an organized, accessible way, and
will enlarge and broaden the audience for our material, as well as
increasing the amount (and quality?) of feedback, leading to greater
improvement of the content.

<!-- handles -->
[@annajiat]: https://github.com/annajiat
[@bkmgit]:   https://github.com/bkmgit
[@haozeke]:  https://github.com/haozeke
[@ocaisa]:   https://github.com/ocaisa
[@reid-a]:   https://github.com/reid-a
[@tkphd]:    https://github.com/tkphd
[@wirawan0]: https://github.com/wirawan0

<!-- affiliations -->
[BracU]: https://www.bracu.ac.bd
[CECAM]: https://www.cecam.org
[Kichakato Kizito]: http://kichakatokizito.solutions
[NIST]: https://www.nist.gov
[ODU]: https://www.odu.edu
[Quansight labs]: https://labs.quansight.org
[U. Iceland]: https://english.hi.is

<!-- links -->
[HPC Intro]: https://github.com/carpentries-incubator/hpc-intro
[HPC Workflows]: https://github.com/carpentries-incubator/hpc-workflows
[hpcc-gh]: https://github.com/hpc-carpentry/
[hpcc-sl]: https://swcarpentry.slack.com/archives/CEXAZR52T
[hpcc-tb]: https://carpentries.topicbox.com/groups/discuss-hpc
[hpcc-tw]: https://twitter.com/hpccarpentry
[hpcc-web]: https://www.hpc-carpentry.org
[hpcc-workshops]: http://www.hpc-carpentry.org/past-workshops/
